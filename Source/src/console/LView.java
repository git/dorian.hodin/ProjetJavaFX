package console;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.Objects;

public class LView extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {

        Parent root1 = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/MainPage.fxml")));
        Scene scene1 = new Scene(root1);
        primaryStage.setTitle("Station Météo");
        primaryStage.setWidth(900);
        primaryStage.setHeight(500);
        primaryStage.setScene(scene1);
        primaryStage.show();
    }

}