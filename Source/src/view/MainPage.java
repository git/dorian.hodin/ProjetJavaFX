package view;

import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import model.*;
import java.util.Objects;
import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

public class MainPage {

    @FXML private Pane bottom;
    @FXML private Button FirstButton;
    @FXML private Button SecondButton;
    @FXML private VBox vb1;
    @FXML private VBox vb2;
    @FXML private ImageView open;
    @FXML private final Button FirstCaptorButton = new Button();
    @FXML private final Button SecondCaptorButton = new Button();
    @FXML private final TextField coefCaptor1 = new TextField();
    @FXML private final TextField coefCaptor2 = new TextField();
    @FXML private final Button submitButton1 = new Button("Submit");
    @FXML private final Button submitButton2 = new Button("Submit");
    @FXML private final Label errorLabel = new Label("mich");
    @FXML private final Button DetailsButtonVirtual = new Button("Détails");
    @FXML private final Button DetailsButtonC1 = new Button("Détails");
    @FXML private final Button DetailsButtonC2 = new Button("Détails");

    CPUTemp cpuTemp = new CPUTemp("CPU Captor");
    static CapteurVirtuel cV1 = new CapteurVirtuel("CaptVirt1");
    static Captor c1 = new Captor("c1");
    static Captor c2 = new Captor("c2");

    @FXML
    public void initialize() {

        FirstButton.textProperty().set(cpuTemp.getName());

        SecondButton.textProperty().set(cV1.getName());

        open.setFitWidth(18);
        open.setFitHeight(18);

        FirstButton.setOnAction(e -> {

            bottom.getChildren().clear(); 
            vb1.getChildren().clear();
            vb1.getChildren().add(setDisplayCPUCaptor());

        });

        SecondButton.setOnAction(e -> {

            bottom.getChildren().clear();
            vb1.getChildren().clear();
            vb1.getChildren().add(setDisplayVirtualCaptor());
            bottom.getChildren().add(DetailsButtonVirtual);

            if (!vb2.getChildren().isEmpty()){

                open.setImage(new Image("https://www.citypng.com/public/uploads/preview/download-black-right-triangle-png-31629764954y0662peplm.png"));
                vb2.getChildren().clear();

            }else{

                open.setImage(new Image("https://cdn-icons-png.flaticon.com/512/0/159.png"));
                vb2.getChildren().add(setDisplayFirstCaptor());
                vb2.getChildren().add(setDisplaySecondCaptor());

            }
        });

        FirstCaptorButton.setOnAction(e -> {

            bottom.getChildren().clear();
            vb1.getChildren().clear();
            errorLabel.textProperty().set("");
            vb1.getChildren().add(setDisplayCaptor(c1));
            bottom.getChildren().add(DetailsButtonC1);


        });

        SecondCaptorButton.setOnAction(e -> {

            bottom.getChildren().clear();
            vb1.getChildren().clear();
            errorLabel.textProperty().set("");
            vb1.getChildren().add(setDisplayCaptor(c2));
            bottom.getChildren().add(DetailsButtonC2);

        });


        submitButton1.setOnAction(e -> {
            try {
                if (!(parseInt(coefCaptor1.getText()) >= 0 && parseInt(coefCaptor1.getText()) <= 10)) {
                    errorLabel.textProperty().set("Mettez un coef valide");
                } else {
                    errorLabel.textProperty().set("Opération validée");
                    c1.setCoef(parseInt(coefCaptor1.getText()));
                }
            }
            catch (NumberFormatException n){
                errorLabel.textProperty().set("Mettez un coef valide");
            }

        });

        submitButton2.setOnAction(e -> {
            try{
                if (!(parseInt(coefCaptor2.getText()) >= 0 && parseInt(coefCaptor2.getText()) <= 10)) {
                    errorLabel.textProperty().set(String.valueOf(valueOf(parseInt(coefCaptor2.getText()))));
                }else {
                    errorLabel.textProperty().set("Opération validée");
                    c2.setCoef(parseInt(coefCaptor2.getText()));
                }
            }catch (NumberFormatException n){
                errorLabel.textProperty().set("Mettez un coef valide");
            }

        });

        DetailsButtonC1.setOnAction(e -> {

            ImageVue display = new ImageVue();
            display.setDisplayWindow(c1);

        });

        DetailsButtonC2.setOnAction(e -> {

            ImageVue display = new ImageVue();
            display.setDisplayWindow(c2);

        });

        DetailsButtonVirtual.setOnAction(e -> {
            ImageVue display = new ImageVue();
            display.setDisplayWindowsCaptorVirtual(cV1);
        });

    }



    private VBox setDisplayCPUCaptor() {


        VBox FirstCaptorCPU = new VBox();
        FirstCaptorCPU.setMinSize(565,470);

        ImageView FirstCaptorCPUImage = new ImageView(new Image("https://media.istockphoto.com/id/1204740322/fr/photo/cpu.jpg?s=612x612&w=0&k=20&c=cF154ASgimXysleBPyYsFMI9_GKkkTpDQ0kZTJnm_K0="));

        FirstCaptorCPUImage.setFitWidth(570);

        Label FirstCaptorCPULabel = new Label("Temperature");

        FirstCaptorCPULabel.setFont(new Font("Arial", 30));
        FirstCaptorCPULabel.setMaxWidth(Double.MAX_VALUE);
        FirstCaptorCPULabel.setAlignment(Pos.CENTER);
        FirstCaptorCPULabel.setWrapText(true);

        if (cpuTemp.getTemperature()==0.0){

            FirstCaptorCPULabel.setFont(new Font("Arial", 15));
            FirstCaptorCPULabel.textProperty().set("Impossible de récupérer la temperature du CPU, possiblement car le fichier qui contient cette temperature n'existe pas ou n'est pas au même endroit que celui utilisé lors du développement.");

        }else{

            FirstCaptorCPULabel.textProperty().set("Temperature du CPU : " +  cpuTemp.getTemperature() + "°C");

        }


        FirstCaptorCPU.getChildren().add(FirstCaptorCPUImage);
        FirstCaptorCPU.getChildren().add(FirstCaptorCPULabel);

        ObservableList<CPUTemp> CPUTemperature = FXCollections.observableArrayList(

                captorCPU-> new ObservableValue[]{
                        captorCPU.getTemperatureProperty()
                }

        );

        CPUTemperature.addListener((ListChangeListener.Change<? extends CPUTemp> c) ->{

            while (c.next()){
                if (c.wasUpdated()){
                    FirstCaptorCPULabel.textProperty().set("Temperature du CPU : " +  cpuTemp.getTemperature() + "°C");
                }
            }

        });

        cpuTemp.startThread();

        CPUTemperature.add(cpuTemp);

        return FirstCaptorCPU;
    }

    private HBox setDisplayFirstCaptor(){

        HBox Captor1 = new HBox();

        ImageView FirstCaptorImage = new ImageView(new Image("https://cdn-icons-png.flaticon.com/512/70/70597.png"));
        FirstCaptorImage.setFitHeight(73);
        FirstCaptorImage.setFitWidth(73);

        FirstCaptorButton.setText(c1.getName());
        FirstCaptorButton.setFont(new Font("System", 13));
        FirstCaptorButton.setMaxWidth(Double.MAX_VALUE);
        FirstCaptorButton.setAlignment(Pos.CENTER_LEFT);

        Captor1.getChildren().add(FirstCaptorImage);
        Captor1.getChildren().add(FirstCaptorButton);

        return Captor1;
    }

    private HBox setDisplaySecondCaptor(){

        HBox Captor2 = new HBox();

        ImageView SecondCaptorImage = new ImageView(new Image("https://cdn-icons-png.flaticon.com/512/70/70597.png"));
        SecondCaptorImage.setFitHeight(73);
        SecondCaptorImage.setFitWidth(73);

        SecondCaptorButton.setText(c2.getName());
        SecondCaptorButton.setFont(new Font("System", 13));
        SecondCaptorButton.setMaxWidth(Double.MAX_VALUE);
        SecondCaptorButton.setAlignment(Pos.CENTER_LEFT);

        Captor2.getChildren().add(SecondCaptorImage);
        Captor2.getChildren().add(SecondCaptorButton);

        return Captor2;

    }

    private VBox setDisplayVirtualCaptor(){

        VBox CaptorVirtual = new VBox();

        Label ViewTempVirtualCaptor = new Label("Temperature moyenne des sous-capteurs du capteur virtuel : " + Math.round(cV1.getTempMoyenne()*10.0)/10.0 + "°C");

        ViewTempVirtualCaptor.setFont(new Font("Arial",20));
        ViewTempVirtualCaptor.setMaxWidth(Double.MAX_VALUE);
        ViewTempVirtualCaptor.setAlignment(Pos.CENTER);
        ViewTempVirtualCaptor.setWrapText(true);

        ImageView VirtualCaptorImage = new ImageView(new Image("https://static.thenounproject.com/png/56610-200.png"));

        VirtualCaptorImage.setFitWidth(500);
        VirtualCaptorImage.setFitHeight(300);


        CaptorVirtual.getChildren().add(VirtualCaptorImage);
        CaptorVirtual.getChildren().add(ViewTempVirtualCaptor);

        c1.startThread();
        c2.startThread();

        cV1.addToLesCapteurs(c1);
        cV1.addToLesCapteurs(c2);

        cV1.startThread();

        ObservableList<CapteurVirtuel> VirtualCaptor = FXCollections.observableArrayList(
                VirtCaptor->new ObservableValue[]{
                        VirtCaptor.getTempMoyenneProperty()
                }
        );

        VirtualCaptor.addListener((ListChangeListener.Change<? extends CapteurVirtuel> c) ->{
            while (c.next()){

                if (c.wasUpdated()){

                    Double temp = cV1.getTempMoyenne();

                    if (temp < 0){

                        ViewTempVirtualCaptor.setStyle("-fx-text-fill : blue");

                    } else if (temp > 30) {

                        ViewTempVirtualCaptor.setStyle("-fx-text-fill : red");

                    }else {

                        ViewTempVirtualCaptor.setStyle("-fx-text-fill : black");

                    }
                    ViewTempVirtualCaptor.textProperty().set("Temperature moyenne des sous-capteurs du capteur virtuel : " + Math.round(cV1.getTempMoyenne()*10.0)/10.0 + "°C");
                }
            }
        });

        VirtualCaptor.add(cV1);


        return CaptorVirtual;
    }

    private VBox setDisplayCaptor(Captor cap){

        VBox Captor = new VBox();

        HBox submitCoef = new HBox();

        Label ViewTempCaptor = new Label("Temperature du capteur " + cap.getName() + " : " + Math.round(cap.getTemperature()*10.0)/10.0 + "°C");

        ViewTempCaptor.setFont(new Font("Arial",25));
        ViewTempCaptor.setMaxWidth(Double.MAX_VALUE);
        ViewTempCaptor.setAlignment(Pos.CENTER);
        ViewTempCaptor.setWrapText(true);

        ImageView CaptorImage = new ImageView(new Image("https://static.thenounproject.com/png/7561-200.png"));

        CaptorImage.setFitWidth(500);
        CaptorImage.setFitHeight(300);



        Label coefLabel = new Label("Changez le coef (de 1 à 10) : ");
        coefLabel.setFont(new Font("Arial",20));

        submitCoef.getChildren().add(coefLabel);

        if (Objects.equals(cap.getName(), "c1")) {
            submitCoef.getChildren().add(coefCaptor1);
            submitCoef.getChildren().add(submitButton1);
        } else if (Objects.equals(cap.getName(), "c2")) {
            submitCoef.getChildren().add(coefCaptor2);
            submitCoef.getChildren().add(submitButton2);
        }

        Captor.getChildren().add(CaptorImage);
        Captor.getChildren().add(ViewTempCaptor);
        Captor.getChildren().add(submitCoef);
        Captor.getChildren().add(errorLabel);

        ObservableList<Captor> CaptorObs = FXCollections.observableArrayList(
                TrueCaptor->new ObservableValue[]{
                        TrueCaptor.getTemperatureProperty()
                }
        );

        CaptorObs.addListener((ListChangeListener.Change<? extends Captor> c) ->{
            while (c.next()){

                if (c.wasUpdated()){

                    Double temp = cap.getTemperature();

                    if (temp < 0){

                        ViewTempCaptor.setStyle("-fx-text-fill : blue");

                    } else if (temp > 30) {

                        ViewTempCaptor.setStyle("-fx-text-fill : red");

                    }else {

                        ViewTempCaptor.setStyle("-fx-text-fill : black");

                    }
                    ViewTempCaptor.textProperty().set("Temperature du capteur " + cap.getName() + " : " + Math.round(cap.getTemperature()*10.0)/10.0 + "°C");
                }
            }
        });

        CaptorObs.add(cap);

        return Captor;
    }

}
