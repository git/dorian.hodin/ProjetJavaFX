package view;

import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.CapteurVirtuel;
import model.Captor;
import javafx.scene.image.Image;
import static javafx.application.Platform.exit;

public class ImageVue {

    @FXML private ImageView img = new ImageView(new Image("https://media.istockphoto.com/id/1125717763/fr/vectoriel/ic%C3%B4ne-de-chargement-design-plat-de-vector-illustration.jpg?s=612x612&w=0&k=20&c=3bkStusySjnEyXtfik1gyjWJqKKd-xOhhMizNy9F-W8="));
    @FXML private Label imgLabel = new Label();
    @FXML private Button fermer = new Button("Fermer");

    public void setDisplayWindowsCaptorVirtual(CapteurVirtuel cv){
        setDisplayBothDisplay();

        ObservableList<CapteurVirtuel> VirtualCaptor = FXCollections.observableArrayList(
                TrueCaptor->new ObservableValue[]{
                        TrueCaptor.getTempMoyenneProperty()
                }
        );

        VirtualCaptor.addListener((ListChangeListener.Change<? extends CapteurVirtuel> c) ->{
            while (c.next()){

                if (c.wasUpdated()){

                    try {

                        Double temp = cv.getTempMoyenne();

                        if (temp < 0) {

                            img.setImage(new javafx.scene.image.Image("https://static.vecteezy.com/ti/vecteur-libre/p1/1555350-montagne-enneigee-paysage-d-hiver-avec-flocons-de-neige-gratuit-vectoriel.jpg"));

                        } else if (temp > 30) {

                            img.setImage(new javafx.scene.image.Image("https://png.pngtree.com/thumb_back/fw800/background/20220515/pngtree-cartoon-landscape-with-house-and-summer-nature-scene-image_1367386.jpg"));

                        } else {

                            img.setImage(new javafx.scene.image.Image("https://www.tunisienumerique.com/wp-content/uploads/2023/01/meteo-nuage-1-compressed-1.jpg"));

                        }
                        imgLabel.textProperty().set(Math.round(temp * 10.0) / 10.0 + "°C");
                    }catch (NullPointerException ignored){}
                }
            }
        });

        VirtualCaptor.add(cv);
    }

    public void setDisplayWindow(Captor cap){

        setDisplayBothDisplay();

        ObservableList<Captor> CaptorObs = FXCollections.observableArrayList(
                TrueCaptor->new ObservableValue[]{
                        TrueCaptor.getTemperatureProperty()
                }
        );

        CaptorObs.addListener((ListChangeListener.Change<? extends Captor> c) ->{
            while (c.next()){

                if (c.wasUpdated()){

                    try {

                        Double temp = cap.getTemperature();

                        if (temp < 0) {

                            img.setImage(new javafx.scene.image.Image("https://static.vecteezy.com/ti/vecteur-libre/p1/1555350-montagne-enneigee-paysage-d-hiver-avec-flocons-de-neige-gratuit-vectoriel.jpg"));

                        } else if (temp > 30) {

                            img.setImage(new javafx.scene.image.Image("https://png.pngtree.com/thumb_back/fw800/background/20220515/pngtree-cartoon-landscape-with-house-and-summer-nature-scene-image_1367386.jpg"));

                        } else {

                            img.setImage(new javafx.scene.image.Image("https://www.tunisienumerique.com/wp-content/uploads/2023/01/meteo-nuage-1-compressed-1.jpg"));

                        }
                        imgLabel.textProperty().set(Math.round(temp * 10.0) / 10.0 + "°C");
                    }catch (NullPointerException ignored){}
                }
            }
        });

        CaptorObs.add(cap);
    }

    private void setDisplayBothDisplay() {
        VBox vue = new VBox();
        vue.setMinSize(600,400);

        img.setFitWidth(600);
        img.setFitHeight(400);

        imgLabel.setFont(new Font("Arial", 25));

        vue.getChildren().add(img);
        vue.getChildren().add(imgLabel);
        vue.getChildren().add(fermer);

        fermer.setOnAction(e -> exit());

        Scene SecondScene = new Scene(vue);

        Stage Image = new Stage();
        Image.setTitle("Image");
        Image.setScene(SecondScene);
        Image.show();
    }
}

