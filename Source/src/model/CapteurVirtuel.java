package model;


import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.*;
import java.util.*;

public class CapteurVirtuel implements Runnable {

    private final StringProperty nom;
    private final DoubleProperty tempMoyenne = new SimpleDoubleProperty(0);

    final Map<Integer, Captor> source = new HashMap<>();
    private final ObservableMap<Integer,Captor> lesCapteurs = FXCollections.observableMap(source);
    public CapteurVirtuel(String nom){
        this.nom= new SimpleStringProperty(nom);
        this.tempMoyenne.set(0.0);
        lesCapteurs.addListener((MapChangeListener<? super Integer, ? super Captor>) c -> this.updateData());
    }

    public DoubleProperty getTempMoyenneProperty(){
        return this.tempMoyenne;
    }

    public Double getTempMoyenne(){
        return this.tempMoyenne.get();
    }


    public void addToLesCapteurs(Captor c){
        lesCapteurs.put(c.getCoef(),c);
    }

    private void updateData() {
        int coef = 0;
        double valeurs =0;
        for (Map.Entry<Integer, Captor> entry : lesCapteurs.entrySet()){
            coef += entry.getKey();
            valeurs += entry.getKey() * entry.getValue().getTemperature();
        }
        if (coef==0 || valeurs==0){
            this.tempMoyenne.set(0.0);
        }else {
            this.tempMoyenne.set(valeurs/coef);
        }
    }

    @Override
    public void run(){
        while (true){
            Platform.runLater(this::updateData);
            try{
                Thread.sleep(2000);
            }
            catch (InterruptedException e){
                break;
            }
        }
    }

    public void startThread(){
        Thread threadVirtualCaptor = new Thread(this);
        threadVirtualCaptor.setDaemon(true);
        threadVirtualCaptor.start();
    }

    public String getName() {
        return this.nom.get();
    }
}
