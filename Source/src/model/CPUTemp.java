package model;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.io.*;

public class CPUTemp implements Runnable {

    DoubleProperty temperature = new SimpleDoubleProperty(0.0);

    String name ;

    public CPUTemp(String nom) {
        this.name=nom;
        genererTemperature();
    }
    public void genererTemperature(){
        try{
            File temp = new File("/sys/devices/virtual/thermal/thermal_zone6/temp");
            BufferedReader br = new BufferedReader(new FileReader(temp));
            setTemperature((double) (Float.parseFloat(br.readLine())/1000));
        }catch (IOException e){
            setTemperature(0.0);
        }
    }

    public void setTemperature(Double temperature){
        this.temperature.set(temperature);
    }

    public DoubleProperty getTemperatureProperty(){
        return this.temperature;
    }

    public double getTemperature(){
        return this.temperature.get();
    }

    @Override
    public void run(){
        while (true){
            Platform.runLater(this::genererTemperature);
            try{
                Thread.sleep(100);
            }
            catch (InterruptedException e){
                break;
            }
        }
    }

    public void startThread(){
        Thread threadCaptor = new Thread(this);
        threadCaptor.setDaemon(true);
        threadCaptor.start();
    }

    public String getName() {
        return this.name;
    }
}
