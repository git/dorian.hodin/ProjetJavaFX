package model;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import java.util.Random;

public class Captor implements Runnable{

    private final StringProperty name;

    private final DoubleProperty temperature;

    private Integer coef;

    static final Random rand=new Random(System.currentTimeMillis());

    public Captor(String name){
        this.name=new SimpleStringProperty(name);
        this.temperature=new SimpleDoubleProperty(5.0);
        this.coef =rand.nextInt()%10;
    }

    public String getName(){
        return this.name.get();
    }

    public Integer getCoef(){
        return this.coef;
    }

    public void setCoef(Integer i){
        this.coef = i;
    }



    public DoubleProperty getTemperatureProperty(){
        return this.temperature;
    }

    public Double getTemperature(){
        return this.temperature.get();
    }

    public void setTemperature(double temperature){
        this.temperature.set(temperature);
    }

    @Override
    public void run(){
        while (true){
            Platform.runLater(()->setTemperature(rand.nextDouble(80.)-20.));
            try{
                Thread.sleep(2000);
            }
            catch (InterruptedException e){
                break;
            }
        }
    }

    public void startThread(){
        Thread threadCaptor = new Thread(this);
        threadCaptor.setDaemon(true);
        threadCaptor.start();
    }
}
